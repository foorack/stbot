package com.foorack.stbot;

import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.message.*;
import net.dv8tion.jda.core.events.role.*;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.*;

import lombok.*;

/**
 * Java Discord test bot.
 */
public class STBot extends ListenerAdapter {

    // Using lombok's SneakyThrows to handle any Exceptions. This is a prototype so we don't care about error-handling.
    @SneakyThrows
    public static void main(String[] args) {

        //We construct a builder for a BOT account. If we wanted to use a CLIENT account we would use AccountType.CLIENT
        JDA jda = new JDABuilder(AccountType.CLIENT)
        .setToken(args[0])
        .addEventListener(new STBot())
        .buildBlocking();

    }

    // Implemented two ways of triggering the list update. Either via the command <code>!list countries</code> or by doing any update to the roles.

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        MessageChannel channel = event.getChannel();
        String msg = event.getMessage().getContent();

        if (event.isFromType(ChannelType.TEXT)) {
            Guild guild = event.getGuild();
            if (msg.equals("!list countries")) {
                listCountries(guild, channel);
            }
        }
    }

    @Override
    public void onGenericRole(GenericRoleEvent event) {
        listCountries(event.getGuild(), event.getGuild().getTextChannelById("265789806313865217"));
    }

    /**
     *
     * Lists all country-roles on the specified guild. A country role is any role who matches the following format: "<Country> \uD83C\uDF0E". The country has to be properly formatted with the correct casing. If the country is invalid or does not exist in ISO-3166 then the emoji flag will be printed incorrectly.
     * A custom exception has been added to U.A.E which will be replaced by "United Arab Emirates".
     * Neither of the parameters can be null.
     *
     * @param guild Which guild to fetch roles from.
     * @param channel Which channel the final message should be printed in.
     *
     */
    private void listCountries(@NonNull Guild guild, @NonNull MessageChannel channel) {
        Map<String, Integer> list = new HashMap<>();

        for(Role role : guild.getRoles()) {
            // Check if this role starts with the EARTH_GLOBE_AMERICAS emoji. The emoji is stored as hexcode for compatability with ASCII editors and systems. It is specificly defined in Maven's pom.xml file that it should use UTF-8 but better safe than sorry.
            if(!role.getName().endsWith("\uD83C\uDF0E")) {
                continue;
            }
            int amount = guild.getMembersWithRoles(role).size();
            // Checks if at least one person has this role. Disable this if you want to show all country roles even if no one has it.
            if(amount == 0) {
                continue;
            }
            // Get the name of the country by splitting off the earth emoji.
            String country = role.getName().split(" \uD83C\uDF0E")[0].trim();
            // Custom exception for U.A.E
            if(country.equalsIgnoreCase("U.A.E")) {
                country = "United Arab Emirates";
            }
            // Get the country's ISO code and add output to map. Use a :question: symbol in case ISO code is unknown.
            Locale locale = convertCountryNameToIsoCode(country);
            list.put((locale == null ? ":question:" : ":flag_" + locale.getCountry().toLowerCase() + ":") + " **" + country + ":** " + amount, amount);
        }

        // Sort the countries by the amount of people who have the role
        Map<String, Integer> output = sortByValue(list);

        // Finally join the lines together with a newline character and print the result
        channel.sendMessage(String.join("\n", output.keySet())).queue();

    }

    /**
     *
     * Converts the name of the country to a Java Locale object, only if it exists in ISO-3166.
     *
     * @param countryName Name of the country to check
     * @return Locale Java Locale object representing the country. Null if it doesn't exist.
     *
     */
    private Locale convertCountryNameToIsoCode(@NonNull String countryName) {
        for(String country : Locale.getISOCountries()) {
            Locale l = new Locale("", country);
            if (l.getDisplayCountry().equals(countryName)) {
                return l;
            }
        }
        return null;
    }

    // Abstract Map sorting function found on Stackoverflow.
    // Credit goes to the author of this function. For more information and eventual javadoc, see: https://stackoverflow.com/a/2581754/2479087
    public <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
        Collections.sort(list, Collections.reverseOrder(new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
                return (e1.getValue()).compareTo(e2.getValue());
            }
        }));

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

}
